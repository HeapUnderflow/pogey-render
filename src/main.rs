#![feature(proc_macro_hygiene)]

use std::{
	fs::File,
	io::{self, Write},
};

mod model;
use chrono::Utc;
use model::{Chan, Pogeys, Quotes, State};

const TIMEF: &str = "%a, %e %b %Y %H:%M:%S %Z";

fn page(c: &str) -> io::Result<String> {
	let state: Chan<State> = serde_json::from_reader(File::open("state.json")?).unwrap();

	let quotes: Chan<Quotes> = serde_json::from_reader(File::open("quotes.json")?).unwrap();

	let pogeys: Chan<Pogeys> = serde_json::from_reader(File::open("pogeys.json")?).unwrap();

	let sorted_state = match state.get(c) {
		Some(state) => {
			let mut tmp = state
				.counters
				.iter()
				.map(|(n, c)| (n.as_str(), *c))
				.collect::<Vec<(&str, usize)>>();
			tmp.sort_by(|l, r| r.1.cmp(&l.1));
			Some(tmp)
		},
		None => None,
	};

	let sorted_quotes = match quotes.get(c) {
		Some(quotes) => {
			let mut tmp = quotes.quotes.values().collect::<Vec<_>>();
			tmp.sort_by(|l, r| r.time.cmp(&l.time));
			Some(tmp)
		},
		None => None,
	};

	let sorted_pogeys = match pogeys.get(c) {
		Some(pogeys) => {
			// collect refs into a new array to sort without touching the original
			let mut tmp = pogeys.iter().collect::<Vec<_>>();
			tmp.sort_by(|l, r| l.trigger.cmp(&r.trigger));
			Some(tmp)
		},
		None => None,
	};

	Ok((maud::html! {
		(maud::DOCTYPE)
		html lang="en" {
			head {
				meta charset="UTF-8";
				meta name="viewport" content="width=device-width, initial-scale=1.0";
				meta http-equiv="X-UA-Compatible" content="ie=edge";
				title { "HeapBot - " (c)}
				link rel="stylesheet" href="/static/vendor/bulma.min.css";
				link rel="stylesheet" href="heapbot.css";
			}
			body {
				// h1 class="title" { "HeapBot - " (c) }
				nav class="navbar" role="navigation" {
					div class="navbar-brand" {
						div class="navbar-item" alt="HeapBot" {
							img src="/static/robot.png";
						}
						div class="navbar-item" {
							a href=(format!("https://twitch.tv/{}", &c[1..])) { "HeapBot - " (c) }
						}
					}
				}
				section class="section" {
					div class="container" {
						h2 class="title" id="stats" { "Statistics"}
						div class="columns" {
							div class="column" {
								@if let Some(state_) = sorted_state {
									@if !state_.is_empty() {
										table class="table" {
											thead {
												td { "Rank" }
												td { "Pogey" }
												td { "Count" }
											}
											tbody {
												@for (idx, (name, cnt)) in state_.iter().enumerate() {
													tr {
														td { "#" (idx) }
														td { (name) }
														td { (cnt) }
													}
												}
											}
										}
									} @else {
										div class="notification is-warning" {
											(c) " has no Statistics (yet)."
										}
									}
								} @else {
									div class="notification is-warning" {
										(c) " has no Statistics (yet)."
									}
								}
							}
						}
					}
				}
				section class="section" {
					div class="container" {
						h2 class="title" id="quotes" { "Quotes" }
						div class="columns" {
							div class="column" {
								@if let Some(quotes_) = sorted_quotes {
									@if !quotes_.is_empty() {
										table class="table is-fullwidth" {
											thead {
												td { "Id" }
												td { "Quoter" }
												td { "Quoted" }
												td { "Text" }
											}
											tbody {
												@for quote in quotes_ {
													tr {
														td { (quote.id) }
														td { (quote.quoter) }
														td { (quote.time.format(TIMEF)) }
														td { (quote.text) }
													}
												}
											}
										}
									} @else {
										div class="notification is-warning" {
											(c) " has no Quotes (yet)."
										}
									}
								} @else {
									div class="notification is-warning" {
										(c) " has no Quotes (yet)."
									}
								}
							}
						}
					}
				}

				section class="section" {
					div class="container" {
						h2 class="title" id="pogeys" { "Pogeys" }
						div class="columns" {
							div class="column" {
								@if let Some(pogeys_) = sorted_pogeys {
									@if !pogeys_.is_empty() {
										table class="table" {
											thead {
												td { "Trigger" }
												td { "Response" }
												td { "Minimum Count" }
												td { "Max Count" }
											}
											tbody {
												@for pog in pogeys_ {
													tr {
														td { (pog.trigger) }
														td { (pog.response) }
														td { (pog.start) }
														td { (pog.max) }
													}
												}
											}
										}
									} @else {
										div class="notification is-warning" {
											(c) " has no Pogeys (yet)."
										}
									}
								} @else {
									div class="notification is-warning" {
										(c) " has no Pogeys (yet)."
									}
								}
							}
						}
					}
				}
				footer class="footer" {
					div class="content has-text-centered" {
						"Made by " a href="https://heap.wtf" { strong { "HeapUnderflow" } } ". Rendered " (Utc::now().format(TIMEF))
					}
				}
			}
		}
	})
	.into_string())
}

fn main() {
	env_logger::init();

	log::info!("start");

	let chans: model::Config =
		serde_json::from_reader(File::open("botcfg.json").expect("botcfg->file"))
			.expect("botcfg->format");
	for chn in chans.targets {
		let mut f = File::create(&format!("out/{}.html", &chn[1..])).unwrap();
		write!(f, "{}", page(&chn).unwrap()).unwrap();
	}
}

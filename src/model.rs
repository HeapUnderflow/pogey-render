use chrono::{DateTime, Utc};
use serde::Deserialize;
use std::{
	collections::HashMap,
	ops::{Deref, DerefMut},
};

#[derive(Debug, Deserialize)]
pub struct Chan<T> {
	#[serde(flatten)]
	inner: HashMap<String, T>,
}

impl<T> Deref for Chan<T> {
	type Target = HashMap<String, T>;

	fn deref(&self) -> &Self::Target { &self.inner }
}

impl<T> DerefMut for Chan<T> {
	fn deref_mut(&mut self) -> &mut Self::Target { &mut self.inner }
}

#[derive(Debug, Deserialize)]
pub struct Quotes {
	pub quotes: HashMap<String, Quote>,
}
#[derive(Debug, Deserialize)]
pub struct Quote {
	pub id:     String,
	pub text:   String,
	pub quoter: String,
	pub time:   DateTime<Utc>,
}

use bitflags::bitflags;

bitflags! {
	#[derive(Deserialize)]
	pub struct Flags: u32 {
		const NOTHING                          = 0b0000_0000; // Always There

		const NO_LIMIT                         = 0b0000_0001; // Segment 1
		const NO_COMMANDS                      = 0b0000_0010;
		const NO_QUOTES                        = 0b0000_0100;
		const NO_POGEYS                        = 0b0000_1000;

		const ABUSE                            = 0b0000_0001 << 8; // Segment 2
		const BROADCASTER_CAN_TOGGLE_ABUSE     = 0b0000_0010 << 8;
		const MOD_CAN_TOGGLE_ABUSE             = 0b0000_0100 << 8;
		const BR_MANAGE_PERMISSION             = 0b0000_1000 << 8;
		const MD_MANAGE_PERMISSION             = 0b0001_0000 << 8;

		const BROADCASTER_CAN_POGEY            = 0b0000_0001 << (8 * 2); // Segment 3
		const BROADCASTER_CAN_QUOTE            = 0b0000_0010 << (8 * 2);
		const MOD_CAN_POGEY                    = 0b0000_0100 << (8 * 2);
		const MOD_CAN_QUOTE                    = 0b0000_1000 << (8 * 2);
		const SUB_CAN_POGEY                    = 0b0001_0000 << (8 * 2);
		const SUB_CAN_QUOTE                    = 0b0010_0000 << (8 * 2);

		/// Panic mode, can only be released by manually restarting (cleared on startup)
		const PANIC                            = 0b1000_0000 << (8 * 3); // Segment 4
	}
}

impl Default for Flags {
	fn default() -> Self {
		Flags::BROADCASTER_CAN_TOGGLE_ABUSE
			| Flags::MOD_CAN_TOGGLE_ABUSE
			| Flags::BROADCASTER_CAN_POGEY
			| Flags::BROADCASTER_CAN_QUOTE
			| Flags::BR_MANAGE_PERMISSION
			| Flags::MOD_CAN_QUOTE
	}
}

#[derive(Debug, Deserialize)]
pub struct State {
	pub counters: HashMap<String, usize>,
	pub flags:    Flags,
}

#[derive(Debug, Deserialize)]
pub struct Pogey {
	pub trigger:  String,
	pub response: String,
	pub start:    usize,
	pub max:      usize,
}

pub type Pogeys = Vec<Pogey>;

#[derive(Debug, Deserialize)]
pub struct Config {
	pub targets: Vec<String>,
}
